//Import bộ thư viện Express
const express = require('express');

const router = express.Router();

router.get("/vouchers", (request, response) => {
    response.json({
        message: "GET All Voucher"
    })
});

router.post("/vouchers", (request, response) => {
    response.json({
        message: "POST New Voucher"
    })
});

router.get("/vouchers/:voucherId", (request, response) => {
    let voucherId = request.params.voucherId;
    
    response.json({
        message: "GET Voucher ID = " + voucherId
    })
});

router.put("/vouchers/:voucherId", (request, response) => {
    let voucherId = request.params.voucherId;
    
    response.json({
        message: "PUT Voucher ID = " + voucherId
    })
});

router.delete("/vouchers/:voucherId", (request, response) => {
    let voucherId = request.params.voucherId;
    
    response.json({
        message: "DELETE Voucher ID = " + voucherId
    })
});

//Export dữ liệu thành 1 module
module.exports = router;