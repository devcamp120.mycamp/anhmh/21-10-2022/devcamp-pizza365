//Import bộ thư viện Express
const express = require('express');

const router = express.Router();

router.get("/drinks", (request, response) => {
    response.json({
        message: "GET All Drink"
    })
});

router.post("/drinks", (request, response) => {
    response.json({
        message: "POST New Drink"
    })
});

router.get("/drinks/:drinkId", (request, response) => {
    let drinkId = request.params.drinkId;
    
    response.json({
        message: "GET Drink ID = " + drinkId
    })
});

router.put("/drinks/:drinkId", (request, response) => {
    let drinkId = request.params.drinkId;
    
    response.json({
        message: "PUT Drink ID = " + drinkId
    })
});

router.delete("/drinks/:drinkId", (request, response) => {
    let drinkId = request.params.drinkId;
    
    response.json({
        message: "DELETE Drink ID = " + drinkId
    })
});

//Export dữ liệu thành 1 module
module.exports = router;