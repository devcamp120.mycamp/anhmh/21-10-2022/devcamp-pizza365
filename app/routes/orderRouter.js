//Import bộ thư viện Express
const express = require('express');

const router = express.Router();

router.get("/orders", (request, response) => {
    response.json({
        message: "GET All Order"
    })
});

router.post("/orders", (request, response) => {
    response.json({
        message: "POST New Order"
    })
});

router.get("/orders/:orderId", (request, response) => {
    let orderId = request.params.orderId;
    
    response.json({
        message: "GET Order ID = " + orderId
    })
});

router.put("/orders/:orderId", (request, response) => {
    let orderId = request.params.orderId;
    
    response.json({
        message: "PUT Order ID = " + orderId
    })
});

router.delete("/orders/:orderId", (request, response) => {
    let orderId = request.params.orderId;
    
    response.json({
        message: "DELETE Order ID = " + orderId
    })
});

//Export dữ liệu thành 1 module
module.exports = router;