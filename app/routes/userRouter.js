//Import bộ thư viện Express
const express = require('express');

const router = express.Router();

router.get("/users", (request, response) => {
    response.json({
        message: "GET All User"
    })
});

router.post("/users", (request, response) => {
    response.json({
        message: "POST New User"
    })
});

router.get("/users/:userId", (request, response) => {
    let userId = request.params.userId;
    
    response.json({
        message: "GET User ID = " + userId
    })
});

router.put("/users/:userId", (request, response) => {
    let userId = request.params.userId;
    
    response.json({
        message: "PUT User ID = " + userId
    })
});

router.delete("/users/:userId", (request, response) => {
    let userId = request.params.userId;
    
    response.json({
        message: "DELETE User ID = " + userId
    })
});

//Export dữ liệu thành 1 module
module.exports = router;